"""
Module containing two stellar type dicts:
    - STELLAR_TYPE_DICT: dictionary with long names
    - STELLAR_TYPE_DICT_SHORT: dictionary with short names/abbreviations
"""

STELLAR_TYPE_DICT = {
    0: "low mass main sequence",
    1: "Main Sequence",
    2: "Hertzsprung Gap",
    3: "First Giant Branch",
    4: "Core Helium Burning",
    5: "Early Asymptotic Giant Branch",
    6: "Thermally Pulsing",
    7: "NAKED_MAIN_SEQUENCE_HELIUM_STAR",
    8: "NAKED_HELIUM_STAR_HERTZSPRUNG_GAP",
    9: "NAKED_HELIUM_STAR_GIANT_BRANCH",
    10: "HELIUM_WHITE_DWARF",
    11: "CARBON_OXYGEN_WHITE_DWARF",
    12: "OXYGEN_NEON_WHITE_DWARF",
    13: "NEUTRON_STAR",
    14: "BLACK_HOLE",
    15: "MASSLESS REMNANT",
}

STELLAR_TYPE_DICT_SHORT = {
    0: "LMMS",
    1: "MS",
    2: "HG",
    3: "FGB",
    4: "CHeb",
    5: "EAGB",
    6: "TPAGB",
    7: "HeMS",
    8: "HeHG",
    9: "HeGB",
    10: "HeWD",
    11: "COWD",
    12: "ONeWD",
    13: "NS",
    14: "BH",
    15: "MR",
}
