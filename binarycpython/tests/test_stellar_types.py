"""
Unittests for stellar_types module
"""

import unittest

from binarycpython.utils.stellar_types import STELLAR_TYPE_DICT, STELLAR_TYPE_DICT_SHORT

if __name__ == "__main__":
    unittest.main()
