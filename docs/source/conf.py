"""
Configuration file for the Sphinx documentation builder.

This file only contains a selection of the most common options. For a full
list see the documentation:
    https://www.sphinx-doc.org/en/master/usage/configuration.html
    https://brendanhasz.github.io/2019/01/05/sphinx.html
    https://www.sphinx-doc.org/en/1.5/ext/example_google.html


https://stackoverflow.com/questions/22256995/restructuredtext-in-sphinx-and-docstrings-single-vs-double-back-quotes-or-back
"""

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import os
import sys

import m2r2
import jinja2

from git import Repo

from binarycpython.utils.functions import (
    write_binary_c_parameter_descriptions_to_rst_file,
    call_binary_c_config,
)

from binarycpython.utils.grid import Population


def generate_browser_url(ssh_url, branch=None):
    """
    Function to generate the browser url for a git repo
    """

    base = ssh_url.split("@")[-1].replace(".git", "").replace(":", "/").strip()

    if not branch:
        return "https://{}".format(base)
    else:
        return "https://{}/-/tree/{}".format(base, branch)


def write_custom_footer():
    """
    Function to write the custom footer to the template file
    """

    TARGET_DIR = "./_templates"

    output_text = """
{{% extends '!footer.html' %}}

{{%- block extrafooter %}}
<br><br>
Generated on binarycpython git branch: {binarycpython_git_branch} git revision {binarycpython_git_revision} url: <a href="{binarycpython_git_url}">git url</a>.
<br><br>
Using binary_c with bit branch {binary_c_git_branch}: git revision: {binary_c_git_revision} url: <a href="{binary_c_git_url}">git url</a>.

{{% endblock %}}
"""

    binary_c_git_branch = call_binary_c_config("git_branch").strip()
    binary_c_git_revision = call_binary_c_config("git_revision").strip()
    binary_c_git_url = generate_browser_url(
        call_binary_c_config("git_url").strip(), binary_c_git_branch
    )

    # for binarycpython git
    base_dir = os.path.dirname(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    )
    local_repo = Repo(path=base_dir)

    binarycpython_git_branch = local_repo.active_branch.name
    binarycpython_git_revision = local_repo.active_branch.commit
    binarycpython_git_url = generate_browser_url(
        local_repo.remotes.origin.url, binarycpython_git_branch
    )

    formatted_text = output_text.format(
        binarycpython_git_branch=binarycpython_git_branch,
        binarycpython_git_revision=binarycpython_git_revision,
        binarycpython_git_url=binarycpython_git_url,
        binary_c_git_branch=binary_c_git_branch,
        binary_c_git_revision=binary_c_git_revision,
        binary_c_git_url=binary_c_git_url,
    ).strip()

    # Write to file
    with open("_templates/footer.html", "w") as outfile_filehandle:
        outfile_filehandle.write(formatted_text)


#
def patched_m2r2_setup(app):
    """
    Function to handle the markdown parsing better
    """

    try:
        return current_m2r2_setup(app)
    except (AttributeError):
        app.add_source_suffix(".md", "markdown")
        app.add_source_parser(m2r2.M2RParser)
    return dict(
        version=m2r2.__version__,
        parallel_read_safe=True,
        parallel_write_safe=True,
    )


# Include paths for python code
sys.path.insert(0, os.path.abspath("."))
sys.path.insert(0, os.path.join(os.getenv("BINARY_C"), "src/API/"))

# include paths for c code
cautodoc_root = os.path.abspath("../../")

# -- Project information -----------------------------------------------------

project = "binary_c-python"
copyright = "2021, David Hendriks, Robert Izzard"
author = "David Hendriks, Robert Izzard"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.todo",
    "sphinx.ext.coverage",
    "sphinx.ext.viewcode",
    "sphinx.ext.napoleon",
    "hawkmoth",
    "m2r2",
    "sphinx_rtd_theme",
    "sphinx_autodoc_typehints",  # https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html
    "nbsphinx",
]


# Napoleon settings
napoleon_google_docstring = (
    True  # https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html
)
napoleon_numpy_docstring = False
napoleon_include_init_with_doc = False
napoleon_include_private_with_doc = False
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_param = True
napoleon_use_rtype = True

source_suffix = [".rst", ".md"]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

# html_theme = "alabaster"
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]


"""Patching m2r2"""
current_m2r2_setup = m2r2.setup

#
m2r2.setup = patched_m2r2_setup

# Generate some custom documentations for this version of binarycpython and binary_c
docs_pop = Population()

print("Generating binary_c_parameters.rst")
write_binary_c_parameter_descriptions_to_rst_file("binary_c_parameters.rst")
print("Done")

print("Generating grid_options_descriptions.rst")
docs_pop.write_grid_options_to_rst_file("grid_options_descriptions.rst")
print("Done")

# Generate a custom footer
print("Generating custom footer")
write_custom_footer()
print("Done")
