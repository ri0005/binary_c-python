.. binary_c-python documentation master file, created by
   sphinx-quickstart on Wed Nov 13 11:43:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to binary_c-python's documentation!
===========================================
.. mdinclude:: ../../README.md


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme_link
   modules
   example_notebooks
   binary_c_parameters
   grid_options_descriptions
   Visit the GitLab repo <https://gitlab.eps.surrey.ac.uk/ri0005/binary_c-python>
   Submit an issue <https://gitlab.com/binary_c/binary_c-python/-/issues/new>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

