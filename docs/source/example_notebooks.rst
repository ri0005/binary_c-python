Example notebooks
=================
We have a set of notebooks that explain and show the usage of the binarycpython features. The notebooks are also stored in the examples/ directory in the `repository <https://gitlab.eps.surrey.ac.uk/ri0005/binary_c-python/-/tree/master/examples>`_

The order of the notebooks below is more or less the recommended order to read. The last couple of notebooks are example usecases

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    
    examples/notebook_individual_systems.ipynb
    examples/notebook_custom_logging.ipynb
    examples/notebook_population.ipynb
    examples/notebook_extra_features.ipynb
    examples/notebook_api_functionality.ipynb
    examples/notebook_ensembles.ipynb

    examples/notebook_luminosity_function_single.ipynb
    examples/notebook_luminosity_function_binaries.ipynb
    examples/notebook_HRD.ipynb
    examples/notebook_common_envelope_evolution.ipynb
    examples/notebook_BHBH.ipynb
    examples/notebook_solar_system.ipynb
