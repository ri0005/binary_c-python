Population class extension: metadata module
===========================================

.. automodule:: binarycpython.utils.population_extensions.metadata
   :members:
   :undoc-members:
   :show-inheritance:
