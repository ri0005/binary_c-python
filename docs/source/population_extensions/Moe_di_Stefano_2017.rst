Population class extension: Moe\_di\_Stefano\_2017 module
=========================================================

.. automodule:: binarycpython.utils.population_extensions.Moe_di_Stefano_2017
   :members:
   :undoc-members:
   :show-inheritance:
