Population class extension: version\_info module
================================================

.. automodule:: binarycpython.utils.population_extensions.version_info
   :members:
   :undoc-members:
   :show-inheritance:
