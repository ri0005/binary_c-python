Population class extension: distribution\_functions module
==========================================================

.. automodule:: binarycpython.utils.population_extensions.distribution_functions
   :members:
   :undoc-members:
   :show-inheritance:
