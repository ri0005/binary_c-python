Population class extension: condor module
=========================================

.. automodule:: binarycpython.utils.population_extensions.condor
   :members:
   :undoc-members:
   :show-inheritance:
