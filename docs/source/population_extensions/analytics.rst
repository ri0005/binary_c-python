Population class extension: analytics module
============================================

.. automodule:: binarycpython.utils.population_extensions.analytics
   :members:
   :undoc-members:
   :show-inheritance:
