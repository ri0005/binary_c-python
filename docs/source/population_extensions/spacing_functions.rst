Population class extension: spacing\_functions module
=====================================================

.. automodule:: binarycpython.utils.population_extensions.spacing_functions
   :members:
   :undoc-members:
   :show-inheritance:
