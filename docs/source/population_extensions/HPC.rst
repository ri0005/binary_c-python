Population class extension: HPC module
======================================

.. automodule:: binarycpython.utils.population_extensions.HPC
   :members:
   :undoc-members:
   :show-inheritance:
