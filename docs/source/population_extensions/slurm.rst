Population class extension: slurm module
========================================

.. automodule:: binarycpython.utils.population_extensions.slurm
   :members:
   :undoc-members:
   :show-inheritance:
