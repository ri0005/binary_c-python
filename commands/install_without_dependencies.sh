#!/bin/bash
# NOTE: this script needs revision. Does not work properly currently

# Script to install binarycpython in the current venv
VERSION_NUMBER=$(cat "VERSION")
echo "installing binarcpython version $VERSION_NUMBER"

# Clean up all the stuff from before
python setup.py clean --all

# Go into a directory that doesnt contain 'binarycpython' so pip will uninstall the one in the venv, not the local one.
cd src
pip uninstall -y binarycpython
cd ../

# Create build, sdist and install it into the venv
python setup.py build --force
python setup.py sdist
pip install --ignore-installed --no-dependencies -v dist/binarycpython-$VERSION_NUMBER.tar.gz
