#!/bin/bash

# Script to install binarycpython in the current venv

VERSION_NUMBER=$(cat "VERSION")
echo "installing binarcpython version $VERSION_NUMBER"

# we can only use python3 and python3, but allow
# the user to set these in environment variables
# PYTHON and PIP.
: "${PYTHON:="python3"}"
: "${PIP:="pip3"}"

# do stuff...
$PYTHON setup.py clean
$PIP uninstall binarycpython
$PYTHON setup.py build --force
$PYTHON setup.py sdist
$PIP install -v dist/binarycpython-$VERSION_NUMBER.tar.gz
